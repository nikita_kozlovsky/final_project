import { createLoadingSpinner } from './page/other/loading-spinner/loading-spinner.js';
import { data } from './page/other/local-storage.js';
import { checkLoggedUser } from './page/other/check-logged-user.js';
import { getTheme } from './page/other/get-theme.js';
import { createHeader } from './page/header/header.js';
import { createContent } from './page/content/content.js';
import { createFooter } from './page/footer/footer.js';

const loadingSpinner = createLoadingSpinner();
document.body.append(loadingSpinner);

const delay = ms => {
    return new Promise(r => setTimeout(() => r(), ms));
};

async function getPage() {
    try {
        await data();
        const user = await checkLoggedUser();
        await getTheme(user);
        await delay(1500);
        await createHeader(user);
        await createContent(user);
        await createFooter();
    } catch (e) {
        console.error(e);
    }
    finally {
        loadingSpinner.remove();
    }
}

getPage();


// https://kozlovsky-project.000webhostapp.com/

// https://scontent-waw1-1.cdninstagram.com/v/t51.2885-15/e35/p1080x1080/94467557_104807247799609_5933509429405413902_n.jpg?_nc_ht=scontent-waw1-1.cdninstagram.com&_nc_cat=101&_nc_ohc=Yb6LHdNDucIAX_BISyg&oh=317da13b7b3af6fbcd74c27c5a007386&oe=5F7B4C2C