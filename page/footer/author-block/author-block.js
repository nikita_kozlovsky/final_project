import { createTag } from '/page/other/create-element.js';

export const createAuthorBlock = () => {
    const div = createTag('div', 'author-wrap', '');
    const span = createTag('span', '', 'Nikita Kozlovsky, 2020', div);

    return div;
};