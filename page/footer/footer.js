import { createTag } from '/page/other/create-element.js';
import { createAuthorBlock } from './author-block/author-block.js';
import { createSocialBlock } from './social-block/social-block.js';

export const createFooter = () => {
    const footer = createTag('footer', 'footer-wrap', '', document.querySelector('#root'));
    const author = createAuthorBlock();
    const social = createSocialBlock();

    footer.append(social, author);

    return footer;
};