import { createLink, createImg } from '/page/other/create-element.js';

export const createSocialLink = (srcLink, imgAlt, srcImg) => {
    const a = createLink(srcLink);
    const img = createImg(srcImg, imgAlt, a);
    a.target = '_blank';

    return a;
};