import { createTag } from '/page/other/create-element.js';
import { createSocialLink } from './social-link.js';

export const createSocialBlock = () => {
    const div = createTag('div', 'social-wrap', '');
    const facebook = createSocialLink('https://www.facebook.com/nikita.kozlovsky.3', 'Facebook', './image/icon-facebook.png');
    const instagram = createSocialLink('https://www.instagram.com/kozlovsky_nikita/', 'Instagram', './image/icon-instagram.png');
    const vk = createSocialLink('https://vk.com/nikita_kozlovsky', 'VK', './image/icon-vk.png');
    const linkedin = createSocialLink('https://www.linkedin.com/in/nikita-kozlovsky/', 'LinkedIn', './image/icon-linkedin.png');

    div.append(facebook, instagram, vk, linkedin);

    return div;
};