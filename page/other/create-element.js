export const createTag = (htmlTag, cssClass, innerText, parentElement) => {
    const newTag = document.createElement(htmlTag);

    if (cssClass === undefined || cssClass === '') {
        cssClass = null;
    } else if (Array.isArray(cssClass)) {
        for (let newClass of cssClass) {
            newTag.classList.add(newClass);
        }
    } else {
        newTag.classList.add(cssClass);
    }

    if (innerText === undefined || innerText === '') {
        innerText = null;
    } else {
        newTag.innerText = innerText;
    }

    if (parentElement === undefined || parentElement === '') {
        parentElement = null;
    } else {
        parentElement.append(newTag);
    }

    return newTag;
};

export const createLink = (href, parentElement) => {
    const newLink = document.createElement('a');
    newLink.href = href;

    if (parentElement === undefined || parentElement === '') {
        parentElement = null;
    } else {
        parentElement.append(newLink);
    }

    return newLink;
};

export const createInput = (type, placeholder, parentElement) => {
    const newInput = document.createElement('input');
    if (type === undefined || type === '') {
        type = null;
    } else {
        newInput.type = type;
    }

    if (placeholder === undefined || placeholder === '') {
        placeholder = null;
    } else {
        newInput.placeholder = placeholder;
    }

    if (parentElement === undefined || parentElement === '') {
        parentElement = null;
    } else {
        parentElement.append(newInput);
    }

    return newInput;
};

export const createButton = (type, innerText, parentElement) => {
    const newButton = document.createElement('button');

    if (type === undefined || type === '') {
        type = null;
    } else {
        newButton.type = type;
    }

    if (innerText === undefined || innerText === '') {
        innerText = null;
    } else {
        newButton.innerText = innerText;
    }

    if (parentElement === undefined || parentElement === '') {
        parentElement = null;
    } else {
        parentElement.append(newButton);
    }

    return newButton;
};

export const createImg = (src, alt, parentElement) => {
    const newImg = document.createElement('img');

    if (src === undefined || src === '') {
        src = null;
    } else {
        newImg.src = src;
    }

    if (alt === undefined || alt === '') {
        alt = null;
    } else {
        newImg.alt = alt;
    }

    if (parentElement === undefined || parentElement === '') {
        parentElement = null;
    } else {
        parentElement.append(newImg);
    }

    return newImg;
};

export const createList = (type, subtype, count, cssClass, subTypeInner, parentElement) => {
    const newList = document.createElement(type);

    for (let i = 0; i < count; i++) {
        const newSublist = document.createElement(subtype);
        if (Array.isArray(subTypeInner)) {
            if (subTypeInner[i] === undefined || subTypeInner[i] === '') {
                newSublist.innerText = null;
            } else if (subTypeInner[i].nodeType === 1) {
                const subTypeInnerClone = subTypeInner[i].cloneNode();
                newSublist.append(subTypeInnerClone);
            } else {
                newSublist.innerText = subTypeInner[i];
            }
        } else if (subTypeInner === undefined) {
            subTypeInner = null;
        } else {
            newSublist.innerText = subTypeInner;
        }

        newList.append(newSublist);
    }

    if (cssClass === undefined || cssClass === '') {
        cssClass = null;
    } else if (Array.isArray(cssClass)) {
        for (let newClass of cssClass) {
            newList.classList.add(newClass);
        }
    } else {
        newList.classList.add(cssClass);
    }

    if (parentElement === undefined || parentElement === '') {
        parentElement = null;
    } else {
        parentElement.append(newList);
    }

    return newList;
};

export const createError = typeError => {
    const messageError = ['This user already exists',
        'This field is required',
        'Password mismatch',
        'Incorrectly filled password \n (only latin letters, numbers and a period)',
        'Can\'t copy password',
        'Incorrect login or password',
        'Image upload failed \n Please try again'
    ];
    const error = createTag('div', 'auth-error', messageError[typeError]);

    return error;
};