export const getTheme = user => {
    const body = document.querySelector('body');

    if (user) {
        const userTheme = localStorage.getUserProperty(user, 'theme');
        if (userTheme === 'light') {
            localStorage.changeSettingProperty('theme', 'light');
        } else {
            localStorage.changeSettingProperty('theme', 'dark');
        }
    }
    if (!body.classList.contains('light-theme') || !body.classList.contains('dark-theme')) {
        if (localStorage.getSettingProperty('theme') === 'light') {
            body.classList.add('light-theme');
        } else {
            body.classList.add('dark-theme');
        }
    }
};