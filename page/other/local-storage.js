export const data = () => {
    const newUser = (login, password) => {
        const user = {
            'id': function () {
                let countUser = 1;
                const localUser = JSON.parse(localStorage.getItem('users'));
                for (let user in localUser) {
                    countUser++;
                }
                return countUser;
            }(),
            'email': login,
            'password': password,
            'name': '',
            'surname': '',
            'city': '',
            'birthday': '',
            'image': './image/icon-user.png',
            'theme': localStorage.getSettingProperty('theme'),
            'date': new Date().toLocaleDateString()
        };
        const newUser = { [login]: JSON.stringify(user) };
        const localUsers = JSON.parse(localStorage.getItem('users'));
        Object.assign(localUsers, newUser);
        localStorage.setItem('users', JSON.stringify(localUsers));
    };

    const changeUserProperty = (login, property, valueProperty) => {
        const localUsers = JSON.parse(localStorage.getItem('users'));
        if (localUsers.hasOwnProperty(login)) {
            const changeUser = JSON.parse(localUsers[login]);
            changeUser[property] = valueProperty;
            const finishUser = { [login]: JSON.stringify(changeUser) };
            Object.assign(localUsers, finishUser);
            localStorage.setItem('users', JSON.stringify(localUsers));
        }
    };

    const getUserProperty = (login, property) => {
        const localUsers = JSON.parse(localStorage.getItem('users'));
        if (localUsers.hasOwnProperty(login)) {
            const user = JSON.parse(localUsers[login]);
            if (user.hasOwnProperty(property)) {
                return user[property];
            }
        }
    };

    const getSettingProperty = property => {
        const setting = JSON.parse(localStorage.getItem('settings'));
        if (setting.hasOwnProperty(property)) {
            return setting[property];
        }
    };

    const changeSettingProperty = (property, valueProperty) => {
        const setting = JSON.parse(localStorage.getItem('settings'));
        setting[property] = valueProperty;
        localStorage.setItem('settings', JSON.stringify(setting));
    };

    if (!localStorage.hasOwnProperty('settings')) {
        const setting = {
            'theme': 'light',
            'loggedUser': '',
            'position': ''
        };

        const serialSetting = JSON.stringify(setting);
        localStorage.setItem('settings', serialSetting);
    }

    if (!localStorage.hasOwnProperty('users')) {
        localStorage.setItem('users', '{}');
    }

    navigator.geolocation.getCurrentPosition(position => {
        const currentPosition = {
            'latitude': position.coords.latitude,
            'longitude': position.coords.longitude
        };
        localStorage.changeSettingProperty('position', JSON.stringify(currentPosition));
    },
        error => {
            if (error.code == error.PERMISSION_DENIED) {
                localStorage.changeSettingProperty('position', '');
                console.error(error);
            }
        }
    );

    Storage.prototype.newUser = newUser;
    Storage.prototype.changeUserProperty = changeUserProperty;
    Storage.prototype.getUserProperty = getUserProperty;
    Storage.prototype.getSettingProperty = getSettingProperty;
    Storage.prototype.changeSettingProperty = changeSettingProperty;
};