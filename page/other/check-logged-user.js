export const checkLoggedUser = () => {
    if (localStorage.getSettingProperty('loggedUser') !== '') {
        return localStorage.getSettingProperty('loggedUser');
    } else {
        return false;
    }
};