const vLetter = password => {
    const reg = new RegExp('[a-z]');
    if (password.match(reg)) {
        return true;
    }
    return false;
};

const vLetterUp = password => {
    const reg = new RegExp('[A-Z]');
    if (password.match(reg)) {
        return true;
    }
    return false;
};

const vNumber = password => {
    const reg = new RegExp('[0-9]');
    if (password.match(reg)) {
        return true;
    }
    return false;
};

const vSymbol = password => {
    const reg = new RegExp('[\.]');
    if (password.match(reg)) {
        return true;
    }
    return false;
};

export const checkLoginInData = login => {
    const localUsers = JSON.parse(localStorage.getItem('users'));
    if (localUsers.hasOwnProperty(login)) {
        return true;
    } else {
        return false;
    }
};

export const checkSecure = password => {
    let wieght = 0;
    if (vLetter(password)) { wieght++; }
    if (vLetterUp(password)) { wieght++; }
    if (vNumber(password)) { wieght++; }
    if (vSymbol(password)) { wieght++; }
    return wieght;
};

export const validatePassword = password => {
    const reg = new RegExp('[а-яА-я!@#$%^:&?*/={}[]|`<>()\'\",_-]');
    if (checkSecure(password) > 0 && !password.match(reg) && password.length > 7) {
        return true;
    }
    return false;
};