import { createTag } from '/page/other/create-element.js';
import { createLogo } from './logo/logo.js';
import { createNavigation } from './navigation/navigation.js';
import { createThemeButton } from './theme-button/theme-button.js';

export const createHeader = user => {
    const header = createTag('header', 'header-wrap', '', document.getElementById('root'));
    const logo = createLogo();
    const navigation = createNavigation(user);
    const themeButton = createThemeButton(user);

    header.append(logo, themeButton, navigation);

    return header;
};





