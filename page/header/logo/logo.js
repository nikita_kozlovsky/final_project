import * as create from '/page/other/create-element.js';

export const createLogo = () => {
    const div = create.createTag('div', 'logo-wrap');
    const a = create.createLink(window.location.href, div);
    const img = create.createImg('image/logo.png', 'logo', a);
    return div;
};