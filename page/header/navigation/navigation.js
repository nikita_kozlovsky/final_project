import { createTag } from '/page/other/create-element.js';
import { createSignButton } from './elements/sign-button.js';
import { createRegButton } from './elements/reg-button.js';
import { createProfileButton } from './elements/profile-button/profile-button.js';
import { createOutButton } from './elements/out-button.js';
import { createFormVerification } from '/page/content/auth-form/auth-form-ver.js';
import { createFormRegistration } from '/page/content/auth-form/auth-form-reg.js';
import { createProfileForm } from '/page/content/profile-form/profile-form.js';

export const createNavigation = user => {
    const nav = createTag('nav', 'nav-wrap');

    let profileButton;
    let outButton;
    let signButton;
    let regButton;

    if (user) {
        profileButton = createProfileButton(user);
        outButton = createOutButton();
        nav.append(profileButton, outButton);
    } else {
        signButton = createSignButton();
        regButton = createRegButton();
        nav.append(signButton, regButton);
    }

    const clickButton = e => {
        const content = document.querySelector('.content-wrap');

        if (e.target === signButton || e.target === regButton || e.target === profileButton) {
            if (content.hasChildNodes()) {
                for (let child of content.children) {
                    child.remove();
                }
                if (e.target === signButton) {
                    const formVerification = createFormVerification();
                    content.append(formVerification);
                } else if (e.target === regButton) {
                    const formRegistation = createFormRegistration();
                    content.append(formRegistation);
                } else if (e.target === profileButton) {
                    const profileForm = createProfileForm(user);
                    content.append(profileForm);
                }
            }
        } else if (e.target === outButton) {
            localStorage.changeSettingProperty('loggedUser', '');
            window.location.reload();
        }
    };

    nav.addEventListener('click', clickButton);

    return nav;
};