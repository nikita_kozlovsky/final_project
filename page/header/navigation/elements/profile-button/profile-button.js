import { createTag } from '/page/other/create-element.js';
import { createProfileImage } from './profile-img.js';

export const createProfileButton = user => {
    const div = createTag('div', 'profile-nav-button-wrap');
    const profileImage = createProfileImage(user);
    const localName = localStorage.getUserProperty(user, 'name');
    const profileName = createTag('div', 'profile-nav-user-name', localName === '' ? user : localName);

    div.append(profileImage, profileName);

    return div;
};