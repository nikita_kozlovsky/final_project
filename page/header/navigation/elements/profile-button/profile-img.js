import { createTag, createImg } from '/page/other/create-element.js';

export const createProfileImage = user => {
    const div = createTag('div', 'profile-nav-image-wrap');
    const localImage = localStorage.getUserProperty(user, 'image');
    const img = createImg(localImage, 'profile-nav-image', div);

    return div;
};