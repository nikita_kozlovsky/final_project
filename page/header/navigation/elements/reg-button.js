import { createTag } from '/page/other/create-element.js';

export const createRegButton = () => {
    const div = createTag('div', 'reg-button-wrap');
    const button = createTag('div', '', 'Register', div);
    return div;
};