import { createTag } from '/page/other/create-element.js';

export const createSignButton = () => {
    const div = createTag('div', 'sign-button-wrap');
    const button = createTag('div', '', 'Sign in', div);
    return div;
};