import { createTag } from '/page/other/create-element.js';

export const createOutButton = () => {
    const div = createTag('div', 'out-button-wrap');
    const button = createTag('div', '', 'Logout', div);
    return div;
};