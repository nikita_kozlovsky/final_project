import { createTag, createImg } from '/page/other/create-element.js';

export const createThemeButton = user => {
    const div = createTag('div', 'theme-button-wrap');
    const theme = localStorage.getSettingProperty('theme');
    const img = createImg(theme === 'light' ? '/image/icon-dark-theme.png' : '/image/icon-light-theme.png', 'theme-image', div);

    const clickThemeButton = function () {
        const header = document.querySelector('header');
        const navigation = document.querySelector('.nav-wrap');

        const changeTheme = () => {
            header.insertBefore(createThemeButton(), navigation);
            setTimeout(() => {
                document.body.classList.toggle('light-theme');
                document.body.classList.toggle('dark-theme');
            }, 600);
        };

        if (theme === 'light') {
            if (user) {
                localStorage.changeUserProperty(user, 'theme', 'dark');
            }
            localStorage.changeSettingProperty('theme', 'dark');
            this.remove();
            changeTheme();
        } else {
            if (user) {
                localStorage.changeUserProperty(user, 'theme', 'light');
            }
            localStorage.changeSettingProperty('theme', 'light');
            this.remove();
            changeTheme();
        }
    };

    div.addEventListener('click', clickThemeButton);

    return div;
};