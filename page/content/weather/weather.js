import * as create from '/page/other/create-element.js';

export function createWeather(user) {
    const delay = ms => {
        return new Promise(r => setTimeout(() => r(), ms));
    };

    const minsk = 'q=Minsk';
    let userCity;
    let currentLocation;

    if (user) {
        if (localStorage.getUserProperty(user, 'city') !== '') {
            userCity = 'q=' + localStorage.getUserProperty(user, 'city');
        } else {
            try {
                const latitude = JSON.parse(localStorage.getSettingProperty('position')).latitude;
                const longitude = JSON.parse(localStorage.getSettingProperty('position')).longitude;
                currentLocation = 'lat=' + latitude + '&lon=' + longitude;
            } catch (e) {
                console.error(e);
            }
        }
    } else {
        try {
            const latitude = JSON.parse(localStorage.getSettingProperty('position')).latitude;
            const longitude = JSON.parse(localStorage.getSettingProperty('position')).longitude;
            currentLocation = 'lat=' + latitude + '&lon=' + longitude;
        } catch (e) {
            console.error(e);
        }
    }

    const div = create.createTag('div', 'weather-wrap', '', document.querySelector('.content-wrap'));

    const loader = create.createList('div', 'div', 2, 'loader', ['Loading weather...']);

    const weatherUrl = `https://api.openweathermap.org/data/2.5/weather?${userCity || currentLocation || minsk}&units=metric&appid=4a4f15dfbab8c4c6ad0594aa91e358b9`;

    async function getWeatherData() {
    div.append(loader);
        try {
            await delay(2000);

            let response = await fetch(weatherUrl);

            if (response.status == 404) {
                response = await fetch(`https://api.openweathermap.org/data/2.5/weather?${minsk}&units=metric&appid=4a4f15dfbab8c4c6ad0594aa91e358b9`);
            }
            const data = await response.json();

            const { temp: currentTemp, feels_like: feelsTemp, pressure: pressure } = data.main;

            const h = create.createTag('h1', '', 'Weather', div);
            const divC = create.createTag('h2', '', data.name, div);
            const divT = create.createList('div', 'span', 2, 'weather-parameters', ['Temperature:', `${parseInt(currentTemp) + ' C°'}`], div);
            const divF = create.createList('div', 'span', 2, 'weather-parameters', ['Feels:', `${parseInt(feelsTemp) + ' C°'}`], div);
            const divP = create.createList('div', 'span', 2, 'weather-parameters', ['Pressure:', `${parseInt(pressure) + ' hPa'}`], div);
            const buttonRefresh = create.createTag('div', 'weather-button-refresh', '', div);
        } catch (e) {
            console.error(e);
        } finally {
            loader.remove();
        }
    }
    getWeatherData();

    return div;
}


