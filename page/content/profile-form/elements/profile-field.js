import { createTag } from '/page/other/create-element.js';
import { createChangeButton } from './change-button.js';

export const createProfileField = (user, fieldName, property) => {
    const divGroup = createTag('div', 'profile-group');
    const divName = createTag('div', 'profile-field', `${fieldName}`, divGroup);
    const divValue = createTag('div', 'profile-field', `${localStorage.getUserProperty(user, property)}`, divGroup);

    if (localStorage.getUserProperty(user, property) === '') {
        divValue.innerText = '—';
    }

    divGroup.append(createChangeButton(user, property));

    return divGroup;
};