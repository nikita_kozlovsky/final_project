import { createTag } from '/page/other/create-element.js';
import { createChangeInput } from './change-input.js';

export const createChangeButton = (user, property) => {
    const div = createTag('div', 'field-change-button', 'Change');

    function clickButton() {
        if (!document.querySelector('input')) {
            div.parentElement.append(createChangeInput(user, property));
            if (div.previousSibling) {
                div.previousSibling.remove();
            } 
            this.remove();
        }
    }

    div.addEventListener('click', clickButton);

    return div;
};