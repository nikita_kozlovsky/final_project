import { createTag, createInput } from '/page/other/create-element.js';
import { createProfileForm } from '/page/content/profile-form/profile-form.js';

export const createChangeInput = (user, property) => {
    const div = createTag('div', 'profile-change-wrap');
    const input = createInput('text', `Enter data`, div);
    const button = createTag('div', 'field-change-button', 'Confirm', div);

    const clickButton = () => {
        if (input.value === '') {
            document.querySelector('.profile-wrap').remove();
            document.querySelector('.content-wrap').append(createProfileForm(user));
        } else if (input.value !== localStorage.getUserProperty(user, property)) {
            localStorage.changeUserProperty(user, property, input.value);
            document.querySelector('.profile-wrap').remove();
            document.querySelector('.content-wrap').append(createProfileForm(user));
        }
    };

    const pressEnter = e => {
        if (e.key == 'Enter') {
            clickButton();
        }
    };

    input.addEventListener('keydown', pressEnter);
    button.addEventListener('click', clickButton);

    return div;
};