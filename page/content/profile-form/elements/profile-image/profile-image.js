import { createTag, createImg } from '/page/other/create-element.js';
import { createImageSource } from './image-source.js';

export const createProfileImage = user => {
    const div = createTag('div', 'profile-image-wrap');
    const img = createImg(localStorage.getUserProperty(user, 'image'), 'profile-image', div);
    const changeButton = createTag('div', 'image-change-button', 'Change', div);

    const clickChangeButton = e => {
        if (e.target === changeButton) {
            const sourceImg = createImageSource(user);
            div.append(sourceImg);
        }
        if (e.target === img) {
            div.classList.toggle('scale-image');
        }
    };

    div.addEventListener('click', clickChangeButton);

    return div;
};