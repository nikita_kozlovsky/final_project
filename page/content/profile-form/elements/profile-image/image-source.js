import * as create from '/page/other/create-element.js';
import { createProfileForm } from '/page/content/profile-form/profile-form.js';

export const createImageSource = (user) => {
    const div = create.createTag('div', 'image-change-window');
    const input = create.createInput('text', 'Specify the path to the picture', div);
    const button = create.createTag('div', 'image-change-window-button', 'Confirm', div);
    const error = create.createError(6);

    function clickButtonChange(e) {
        if (e.target === button) {
            const checkLink = fetch(input.value);
            checkLink
                .then((response) => {
                    if (response.status !== 200) {
                        if (!this.querySelector('.auth-error')) {
                            this.append(error);
                        }
                        e.preventDefault();
                    } else if (response.url === window.location.href) {
                        this.remove();
                    }
                    else {
                        localStorage.changeUserProperty(user, 'image', input.value);
                        this.remove();
                        document.querySelector('.profile-wrap').remove();
                        document.querySelector('.content-wrap').append(createProfileForm(user));
                    }
                })
                .catch(() => {
                    if (!this.querySelector('.auth-error')) {
                        this.append(error);
                    }
                    e.preventDefault();
                });
        }
    }

    function removeError() {
        if (this.querySelector('.auth-error')) {
            this.querySelector('.auth-error').remove();
        }
    }

    div.addEventListener('click', clickButtonChange);
    div.addEventListener('input', removeError);

    return div;
};