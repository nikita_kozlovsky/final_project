import { createTag } from '/page/other/create-element.js';

export const createDateRegistration = user => {
    const wrap = createTag('div', 'profile-date-field');
    const text = createTag('div', '', `Date of registration on the site: ${localStorage.getUserProperty(user, 'date')}`, wrap);
    return wrap;
};
