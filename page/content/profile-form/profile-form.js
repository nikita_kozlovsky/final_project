import { createTag } from '/page/other/create-element.js';
import { createProfileField } from './elements/profile-field.js';
import { createProfileImage } from './elements/profile-image/profile-image.js';
import { createDateRegistration } from './elements/date-field.js';
import { createContent } from '/page/content/content.js';

export const createProfileForm = user => {
    const div = createTag('div', 'profile-wrap', '', '');
    const h1 = createTag('h1', '', `Profile ${user}`, div);
    const exit = createTag('div', 'exit-button', '', div);
    const image = createProfileImage(user);
    const date = createDateRegistration(user);
    const fieldName = createProfileField(user, 'Name', 'name');
    const fieldSurname = createProfileField(user, 'Surname', 'surname');
    const fieldCity = createProfileField(user, 'City', 'city');
    const fieldBirthday = createProfileField(user, 'Birthday', 'birthday');

    function clickOut(e) {
        if (e.target === exit) {
            this.remove();
            createContent(user);
        }
    }

    div.addEventListener('click', clickOut);

    div.append(image, date, fieldName, fieldSurname, fieldCity, fieldBirthday);
    return div;
};