import { createTag } from '/page/other/create-element.js';
import { createWeather } from './weather/weather.js';
import { createFormVerification } from './auth-form/auth-form-ver.js';

export const createContent = user => {
    const main = createTag('main', 'content-wrap', '', document.querySelector('#root'));

    createWeather(user);

    const clickRefresh = e => {
        if (e.target.classList.contains('weather-button-refresh')) {
            e.target.parentElement.remove();
            localStorage.changeSettingProperty('position', '');

            navigator.geolocation.getCurrentPosition((position) => {
                const currentPosition = {
                    'latitude': position.coords.latitude,
                    'longitude': position.coords.longitude
                };
                localStorage.changeSettingProperty('position', JSON.stringify(currentPosition));
                createWeather(user);
            },
                (error) => {
                    if (error.code == error.PERMISSION_DENIED) {
                        localStorage.changeSettingProperty('position', '');
                        createWeather(user);
                    }
                }
            );
        }
    };

    const successReg = e => {
        if (e.target.classList.contains('auth-form') && document.querySelector('.hello-window-wrap')) {
            document.querySelector('.auth-form').remove();
            const formVer = createFormVerification();
            main.append(formVer);
        }
    };

    main.addEventListener('submit', successReg);
    main.addEventListener('click', clickRefresh);

    return main;
};