import { createTag, createList } from '/page/other/create-element.js';

export const createKey = (type = 'e') => {
    const classDiv = type === 'e' ? 'english-layout' :
        type === 'r' ? 'russian-layout' :
            type === 's' ? 'symbol-layout' :
                type === 'n' ? 'number-layout' : null;

    const div = createTag('div', ['key-wrap', classDiv]);

    if (type === 'e') {
        const alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'].forEach((letter) => {
            const enLetter = createList('div', 'span', 1, 'key-letter', letter, div);
        });
    }

    if (type === 'r') {
        const alphabet = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ь', 'ы', 'э', 'ю', 'я'].forEach((letter) => {
            const ruLetter = createList('div', 'span', 1, 'key-letter', letter, div);
        });
    }

    if (type === 's') {
        const symbols = ['!', '@', '#', '$', '%', '&', '?', '-', '+', '=', '~', ';', '*', '(', ')', '[', ']', '{', '}', '\\', '|', '/', ':', '<', '>', ',', '.', '\'', '"', '`'].forEach((symbol) => {
            const sym = createList('div', 'span', 1, 'key-symbol', symbol, div);
        });
    }

    if (type === 'n') {
        const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0].forEach((number) => {
            const num = createList('div', 'span', 1, 'key-number', number, div);
        });
    }

    const clear = createList('div', 'span', 1, 'key-clear', 'Clear', div);
    const backspace = createList('div', 'span', 1, 'key-backspace', '', div);
    const capslock = createList('div', 'span', 1, 'key-caps-lock', 'Caps Lock', div);
    const enter = createList('div', 'span', 1, 'key-enter', 'Enter', div);
    const shift = createList('div', 'span', 1, 'key-shift', 'Shift', div);
    const switchLang = createList('div', 'span', 1, 'key-switch-language', '', div);
    const switchNumb = createList('div', 'span', 1, 'key-switch-number', '123', div);
    const switchSymb = createList('div', 'span', 1, 'key-switch-symbol', '#+=', div);
    const space = createList('div', 'span', 1, 'key-space', 'Space', div);

    return div;
};