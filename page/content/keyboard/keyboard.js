import { createTag } from '/page/other/create-element.js';
import { createKey } from './key.js';
import { showSecure } from '/page/content/auth-form/elements/password-input/check-secure.js';

export const createKeyboard = () => {
    const keyboard = createTag('div', 'keyboard-wrap');
    const key = createKey();

    keyboard.append(key);

    let capsFlag = false;
    let shiftFlag = false;

    function clickKey(e) {
        const keyWrap = keyboard.querySelector('.key-wrap');
        const input = keyboard.parentElement.querySelector('input');

        checkFlags();

        if (e.target.classList.contains('key-switch-language')) {
            if (e.target.parentElement.classList.contains('english-layout')) {
                keyWrap.remove();
                const ruLayout = createKey('r');
                this.append(ruLayout);
                checkFlags();
            } else if (e.target.parentElement.classList.contains('russian-layout')) {
                keyWrap.remove();
                const enLayout = createKey('e');
                this.append(enLayout);
                checkFlags();
            } else {
                keyWrap.remove();
                const enLayout = createKey('e');
                this.append(enLayout);
                checkFlags();
            }
        }

        if (e.target.classList.contains('key-switch-number')) {
            keyWrap.remove();
            const numLayout = createKey('n');
            this.append(numLayout);
            document.querySelector('.key-switch-number').classList.add('key-active');
        }

        if (e.target.classList.contains('key-switch-symbol')) {
            keyWrap.remove();
            const symLayout = createKey('s');
            this.append(symLayout);
            document.querySelector('.key-switch-symbol').classList.add('key-active');
        }

        if (e.target.classList.contains('key-enter')) {
            this.remove();
        }

        if (e.target.classList.contains('key-clear')) {
            input.value = '';
        }

        if (e.target.classList.contains('key-backspace')) {
            input.value = input.value.slice(0, -1);
        }

        if (e.target.classList.contains('key-letter')) {
            input.value += e.target.innerText;
            shiftFlag = false;
        }

        if (e.target.classList.contains('key-number') || e.target.classList.contains('key-symbol')) {
            input.value += e.target.innerText;
        }

        if (e.target.classList.contains('key-space')) {
            input.value += ' ';
        }

        if (e.target.classList.contains('key-caps-lock')) {
            if (capsFlag) {
                capsFlag = false;
            } else {
                capsFlag = true;
            }
        }

        if (e.target.classList.contains('key-shift')) {
            if (shiftFlag) {
                shiftFlag = false;
            } else {
                shiftFlag = true;
            }
        }

        if (document.querySelector('.keyboard-wrap') && this.parentElement.querySelector('.secure-wrap')) {
            showSecure(this.parentElement.querySelector('input').value);
        }

        checkFlags();

        function checkFlags() {
            if (document.querySelector('.keyboard-wrap')) {
                if (capsFlag) {
                    document.querySelector('.key-caps-lock').classList.add('key-active');
                } else {
                    document.querySelector('.key-caps-lock').classList.remove('key-active');
                }

                if (shiftFlag) {
                    document.querySelector('.key-shift').classList.add('key-active');
                } else {
                    document.querySelector('.key-shift').classList.remove('key-active');
                }

                if (!shiftFlag && capsFlag) {
                    for (let key of document.querySelector('.key-wrap').children) {
                        if (key.classList.contains('key-letter')) {
                            key.firstChild.innerText = key.innerText.toUpperCase();
                        }
                    }
                } else if (shiftFlag && !capsFlag) {
                    for (let key of document.querySelector('.key-wrap').children) {
                        if (key.classList.contains('key-letter')) {
                            key.firstChild.innerText = key.innerText.toUpperCase();
                        }
                    }
                } else if (!shiftFlag && !capsFlag) {
                    for (let key of document.querySelector('.key-wrap').children) {
                        if (key.classList.contains('key-letter')) {
                            key.firstChild.innerText = key.innerText.toLowerCase();
                        }
                    }
                } else if (shiftFlag && capsFlag) {
                    for (let key of document.querySelector('.key-wrap').children) {
                        if (key.classList.contains('key-letter')) {
                            key.firstChild.innerText = key.innerText.toLowerCase();
                        }
                    }
                }
            }
        }
    }

    keyboard.addEventListener('click', clickKey);

    return keyboard;
};


