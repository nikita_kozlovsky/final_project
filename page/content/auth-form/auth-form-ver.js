import { createTag, createError } from '/page/other/create-element.js';
import { createInputLogin } from './elements/login-input.js';
import { createInputPassword } from './elements/password-input/password-input.js';
import { createSubmitButton } from './elements/submit-button.js';
import { createHelloWindow } from './elements/hello-window.js';
import { createContent } from '/page/content/content.js';

export const createFormVerification = () => {
    const form = createTag('form', 'auth-form');
    const h1 = createTag('h1', '', 'Sign in', form);
    const login = createInputLogin(true);
    const password = createInputPassword(true);
    const button = createSubmitButton('Sign in');
    const exit = createTag('div', 'exit-button', '', form);

    form.append(login, password, button);

    function main(e) {
        const loginValue = login.querySelector('input').value;
        const passwordValue = password.querySelector('input').value;

        if (this.querySelector('.auth-error')) {
            for (let error of this.querySelectorAll('.auth-error')) {
                error.remove();
            }
        }

        if (localStorage.getUserProperty(loginValue, 'password') === passwordValue) {
            localStorage.changeSettingProperty('loggedUser', loginValue);
            createHelloWindow(loginValue, 'ver');
            e.preventDefault();
        } else {
            password.append(createError(5));
            e.preventDefault();
        }
    }

    function removeError() {
        if (this.querySelector('.auth-error')) {
            this.querySelector('.auth-error').remove();
        }
    }

    function clickOut(e) {
        if (e.target === exit) {
            this.remove();
            createContent();
        }
    }

    form.addEventListener('click', clickOut);
    form.addEventListener('submit', main);
    form.addEventListener('input', removeError);

    return form;
};