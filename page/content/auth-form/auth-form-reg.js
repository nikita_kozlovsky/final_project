import { createTag, createError } from '/page/other/create-element.js';
import { checkLoginInData, validatePassword } from '/page/other/validation.js';
import { createHelloWindow } from './elements/hello-window.js';
import { createInputLogin } from './elements/login-input.js';
import { createInputPassword } from './elements/password-input/password-input.js';
import { createSubmitButton } from './elements/submit-button.js';
import { createContent } from '/page/content/content.js';

export const createFormRegistration = () => {
    const form = createTag('form', 'auth-form');
    const h1 = createTag('h1', '', 'Registration', form);
    const login = createInputLogin(true);
    const password = createInputPassword(true, true);
    const passwordRepeat = createInputPassword(true);
    const submitButton = createSubmitButton('Register');
    const exit = createTag('div', 'exit-button', '', form);

    form.append(login, password, passwordRepeat, submitButton);

    

    function removeError() {
        if (this.querySelector('.auth-error')) {
            this.querySelector('.auth-error').remove();
        }
    }

    function main(e) {
        const loginValue = login.querySelector('input').value;
        const passwordValue = password.querySelector('input').value;
        const passwordRepeatValue = passwordRepeat.querySelector('input').value;

        if (this.querySelector('.auth-error')) {
            for (let error of this.querySelectorAll('.auth-error')) {
                error.remove();
            }
        }

        const addErrorEmptyInput = () => {
            let flag = true;
            if (loginValue === '') {
                login.append(createError(1));
                flag = false;
            }
            if (passwordValue === '') {
                password.append(createError(1));
                flag = false;
            }
            if (passwordRepeatValue === '') {
                passwordRepeat.append(createError(1));
                flag = false;
            }
            return flag;
        };

        const checkUser = () => {
            let flag = true;
            if (checkLoginInData(loginValue)) {
                login.append(createError(0));
                flag = false;
            }
            return flag;
        };

        const checkPassword = () => {
            let flag = true;
            if (!validatePassword(passwordValue) && passwordValue !== '') {
                password.append(createError(3));
                flag = false;
            } else if (passwordValue !== passwordRepeatValue) {
                password.append(createError(2));
                flag = false;
            }
            return flag;
        };

        const saveUser = () => {
            localStorage.newUser(loginValue, passwordValue);
            createHelloWindow('', 'reg');            
        };

        if (!addErrorEmptyInput() || !checkPassword() || !checkUser()) {
            e.preventDefault();
        } else if (checkPassword() && checkUser() && addErrorEmptyInput()) {
            saveUser();
            e.preventDefault();
        }
    }

    function clickOut(e) {
        if (e.target === exit) {
            this.remove();
            createContent();
        }
    }

    form.addEventListener('click', clickOut);
    form.addEventListener('submit', main);
    login.addEventListener('input', removeError);
    password.addEventListener('input', removeError);
    passwordRepeat.addEventListener('input', removeError);

    return form;
};
