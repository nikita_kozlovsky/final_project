import { createTag } from '/page/other/create-element.js';

export const createHelloWindow = (user, value) => {
    const div = createTag('div', 'hello-window-wrap', '', document.querySelector('body'));
    const localName = localStorage.getUserProperty(user, 'name');
    let nameUser = '';

    if (value === 'reg') {
        nameUser = 'You have successfully registered';
        setTimeout(() => {
            div.remove();
        }, 2000);
    } else {
        if (localName === '') {
            nameUser = `Hello ${user}`;
        } else {
            nameUser = `Hello ${localName}`;
        }
        setTimeout(() => {
            window.location.reload();
        }, 2000);
    }

    const helloMessage = createTag('div', '', nameUser, div);

    return div;
};