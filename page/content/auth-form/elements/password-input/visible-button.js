import { createTag } from '/page/other/create-element.js';

export const createButtonVisibility = () => {
    const button = createTag('div', ['auth-password-visible-button', 'show-password']);

    function doVisiblePassword() {
        const input = this.parentElement.querySelector('input');
        if (input.type === 'password') {
            input.type = 'text';
            this.classList.toggle('hidden-password');
            this.classList.toggle('show-password');
        } else {
            input.type = 'password';
            this.classList.toggle('hidden-password');
            this.classList.toggle('show-password');
        }
    }

    button.addEventListener('click', doVisiblePassword);

    return button;
};