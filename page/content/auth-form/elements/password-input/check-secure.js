import { createTag } from '/page/other/create-element.js';
import { checkSecure } from '/page/other/validation.js';

export const createCheckPassword = () => {
    const div = createTag('div', 'secure-wrap');
    const divLine = createTag('div', 'secure-line', '', div);
    const divDescr = createTag('div', 'secure-description', '', div);
    const spanError = createTag('span', 'secure-message', 'Minimum 8 characters', divDescr);
    const spanCount = createTag('span', 'secure-symbol', '', divDescr);

    return div;
};

export function showSecure(input) {
    const messageArray = ['Minimum 8 characters', 'Weak password,', 'Average password,', 'Strong password,', 'Very strong password,'];
    let line;
    let message;
    let count;
    let inputValue;

    if (typeof (input) === "object") {
        message = this.parentElement.querySelector('.secure-message');
        line = this.parentElement.querySelector('.secure-line');
        count = this.parentElement.querySelector('.secure-symbol');
        inputValue = this.parentElement.querySelector('input').value;
    } else {
        message = document.querySelector('.keyboard-wrap').parentElement.querySelector('.secure-message');
        line = document.querySelector('.keyboard-wrap').parentElement.querySelector('.secure-line');
        count = document.querySelector('.keyboard-wrap').parentElement.querySelector('.secure-symbol');
        inputValue = input;
    }

    if (inputValue.length > 0 && inputValue.length < 8) {
        line.classList.add('secure-error');
        line.classList.remove('secure-warning', 'secure-success', 'secure-full-success');
        message.innerText = messageArray[0];
        count.innerText = '';
    } else if (inputValue.length >= 8) {
        count.innerText = `${inputValue.length + ' ' + 'characters'}`;
        if (checkSecure(inputValue) === 1) {
            line.classList.add('secure-error');
            line.classList.remove('secure-warning', 'secure-success', 'secure-full-success');
            message.innerText = messageArray[1];
        } else if (checkSecure(inputValue) === 2) {
            line.classList.remove('secure-error', 'secure-success', 'secure-full-success');
            line.classList.add('secure-warning');
            message.innerText = messageArray[2];
        } else if (checkSecure(inputValue) === 3) {
            line.classList.remove('secure-error', 'secure-warning', 'secure-full-success');
            line.classList.add('secure-success');
            message.innerText = messageArray[3];
        } else if (checkSecure(inputValue) === 4) {
            line.classList.remove('secure-error', 'secure-warning', 'secure-success');
            line.classList.add('secure-full-success');
            message.innerText = messageArray[4];
        } else {
            line.classList.add('secure-error');
            line.classList.remove('secure-warning', 'secure-success', 'secure-full-success');
            message.innerText = messageArray[1];
        }
    } else {
        line.classList.remove('secure-error', 'secure-warning', 'secure-success', 'secure-full-success');
        message.innerText = messageArray[0];
        count.innerText = '';
    }
}
