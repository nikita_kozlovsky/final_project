import { createTag, createInput } from '/page/other/create-element.js';
import { createButtonVisibility } from './visible-button.js';
import { showSecure, createCheckPassword } from './check-secure.js';
import { createKeyboardButton } from '/page/content/auth-form/elements/keyboard-button.js';

export const createInputPassword = (vButton, cPassword, kButton) => {
    const div = createTag('div', 'auth-password-wrap');
    const input = createInput('password', 'Enter password', div);
    const buttonVisible = vButton ? createButtonVisibility() : null;
    const checkPassword = cPassword ? createCheckPassword() : null;
    const button = kButton ? createKeyboardButton() : null;

    if (cPassword) {
        div.append(checkPassword);
        input.addEventListener('input', showSecure);
    }

    if (kButton) {
        div.prepend(button);
    }

    if (vButton) {
        div.prepend(buttonVisible);
    }

    return div;
};