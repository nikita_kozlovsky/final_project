import { createTag } from '/page/other/create-element.js';
import { createKeyboard } from '/page/content/keyboard/keyboard.js';

export const createKeyboardButton = () => {
    const button = createTag('div', 'keyboard-button');

    function doVisibleKeyboard() {
        if (!this.parentElement.querySelector('.keyboard-wrap')) {
            if (document.querySelector('.keyboard-wrap')) {
                document.querySelector('.keyboard-wrap').remove();
            }
            this.parentElement.prepend(createKeyboard());
        } else {
            this.parentElement.querySelector('.keyboard-wrap').remove();
        }
    }

    button.addEventListener('click', doVisibleKeyboard);

    return button;
};