import { createTag, createInput } from '/page/other/create-element.js';
import { createKeyboardButton } from './keyboard-button.js';

export const createInputLogin = (kButton) => {
    const div = createTag('div', 'auth-login-wrap');
    const input = createInput('email', 'Enter e-mail', div);
    const button = kButton ? createKeyboardButton() : null;

    if (kButton) {
        div.prepend(button);
    }
    
    return div;
};