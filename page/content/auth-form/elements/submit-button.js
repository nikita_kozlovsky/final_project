import { createTag, createButton } from '/page/other/create-element.js';

export const createSubmitButton = (text) => {
    const div = createTag('div', 'auth-button-submit-wrap');
    const button = createButton('submit', text, div);

    return div;
};